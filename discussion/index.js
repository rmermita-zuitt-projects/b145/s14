console.log ("Hello from JS");

console.log("end of comment");

let myVariable;

let clientName = "Juan Dela Cruz";
let contactNumber = "1123456798";

// Peeking inside a variable

let greetings = "Hello Batch 145";
let pangalan = "John Doe";
// 1. String
let country = "Philippines";
let province = 'Metro Manila';
// 2. Number
let headcount = 26;
let grade = 98.7;
let planetDistance = 2e10;
// 3. Boolean
let isMarried = false;
let inGoodConduct = true;
// 4. Null
let spouse = null;
console.log(spouse);
let criminalRecords = true;
// 5. Undefined
let fullName;
// 6. Arrays
let grades = [98.6, 92.3, 90.5]
// 7. Objects
let myGrades = {
	firstGrading: 98.6,
	secondGrading: 92.3,
	thirdGrading: 90.5,
}

console.log(clientName);
console.log(contactNumber);
console.log(greetings);
console.log(pangalan);

// 3 Forms of Data Types
// 1. Primitive -> contains only a single value: strings, numbers, boolean
// 2. Composite -> collection of values: arrays, objects
// 3. Special -> null, undefined

// 5. Arrays are a special kind of composite data type that is used to store multiple values.
let bootcampSubjects = ["HTML", "CSS", "Bootstrap", "Javascript"];
console.log(bootcampSubjects);
// an array should be a collection of data that describes a similar/single topic or subject.
let details = ["Keanu", "Reeves", 32, true];
console.log(details);

// Objects are another special kind of composite data type that is used to mimic or represent a real world object/item.
// They are used to create complext data that contains pieces of information that are relevant to each other.
// Every individual piece of code/information is called property of an object.
let cellphone = {
	brand: 'Samsung',
	model: 'A12',
	color: 'Black',
	serialNo: 'ADS3424532',
	isHomeCredit: true,
	features: ["Calling", "Texting", "Ringing", "5G"],
	price: 8000
}

console.log(cellphone);
// syntax
	// let/const objectName = {
	// 	key -> value
	// 	propertyA: value,
	// 	propertyB: value
	// }

// Variables are used to store data.
// the values or info stored inside a variable can be changed or repackaged.
let personName = "Michael";
console.log(personName);
// if you're going to reassign a new value for a variable, you will no longer have to use the "let" again.
personName = "Jordan";
console.log(personName);

//concatenating string (+)
	// join, combine, link
let pet = "dogs";
console.log("this is the initial value of var:" + pet);

pet = "cat";
console.log("This is the new value of the var:" + pet);

//Constant
	// permanent, fixed
// syntax: const desiredName = value;
const PI = 3.14;
console.log(PI);

const year = 12;
console.log(year);

const familyName = 'Dela Cruz';
console.log(familyName);


// OpenClass Solution
let firstName = "John";
let lastName = "Smith";
let age = 30;
let hobbies = ["Biking", "Mountain Climbing", "Swimming"];

function printUserData(firstName, lastName, age, hobbies) {
    console.log("First Name: " + firstName);
    console.log("Last Name: " + lastName);
    console.log("Age: " + age);
    console.log("Hobbies:");
    console.log(hobbies);
}

printUserData(firstName, lastName, age, hobbies);

// Your code here
function getUserAddress(houseNumber, street, city, state) {
    return {houseNumber: houseNumber, street: street, city: city, state: state};
}

let address = getUserAddress("32", "Washington", "Lincoln", "Nebraska");